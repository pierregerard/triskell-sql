SELECT Q3.*,

       CASE
    WHEN Q3.l_lc = "Terminé-Completed" THEN "purple"
    WHEN Q3.l_lc = "Gelé-Frozen" THEN "frozen"
    WHEN Q3.l_exd IS NULL THEN "no_date"
    WHEN Q3.l_exd < NOW() THEN "expectedDate_error"
    WHEN Q3.l_ind IS NULL THEN "no_flag"
    ELSE Q3.flag
    END AS veracity,
    CASE
WHEN Q3.l_exd IS NULL OR Q3.pr_id IS NULL THEN NULL
ELSE DATEDIFF(Q3.l_exd, Q3.pr_dl)
END AS l_vs_pr, 
    CASE
    WHEN Q3.pr_id IS NULL THEN "livrableterritory"
    ELSE "livrableproof"
    END AS category,
CASE
    WHEN Q3.l_lc IN ("Terminé-Completed", "Gelé-Frozen", "Annulé-Canceled") OR Q3.str_status = "Annulé-Canceled" THEN NULL
    WHEN Q3.l_exd IS NULL THEN NULL
    WHEN Q3.pr_id IS NULL THEN NULL
    WHEN Q3.pr_id IS NOT NULL AND Q3.l_exd > Q3.pr_dl THEN "Late"
    WHEN Q3.pr_id IS NOT NULL AND Q3.l_exd < Q3.pr_dl THEN "InTime"
    END AS delay_flag 
 

FROM(

SELECT Q1.*, 
 Q2.*,
MPT.dataobject_name AS mp_n,
PROG.dataobject_id AS p_id,
PROG.dataobject_name AS p_n,
PF.dataobject_id AS pf_id,
PF.dataobject_name AS pf_n,

CASE
         WHEN Q2.pr_lc = "Annulé-Canceled" OR Q2.smp_lc = "Annulé-Canceled" OR Q2.spr_lc = "Annulé-Canceled" THEN false ELSE true END AS str_status,

 CASE
        WHEN PF.dataobject_name LIKE 'SA%' THEN false
        ELSE true
        END AS liv_country,

CASE
        WHEN Q2.pr_id IS NULL THEN NULL
        WHEN Q2.spr_lc = "Annulé-Canceled" AND Q1.l_lc != "Annulé-Canceled" THEN "program_canceled"
        WHEN Q2.smp_lc = "Annulé-Canceled" AND Q1.l_lc != "Annulé-Canceled" THEN "mainproject_canceled"
        WHEN Q2.pr_lc = "Annulé-Canceled" AND Q1.l_lc != "Annulé-Canceled" THEN "proof_canceled"
        ELSE NULL
        END AS link_l_pr,
        CASE
        WHEN Q1.l_lc IN ("Gelé-Frozen", "Terminé-Completed", "Annulé-Canceled" ) THEN NULL
        WHEN Q1.l_t_d IS NULL THEN NULL
        WHEN Q1.l_c_d IS NULL THEN NULL
        WHEN Q1.l_t_d > Q1.l_c_d THEN "Late"
        ELSE "In Time"
        END AS commitvstgt,
        CASE Q1.l_lc
        WHEN "Terminé-Completed" THEN Q1.l_end ELSE Q1.l_t_d
        END AS l_exd,
        CASE
        WHEN Q1.l_lc = "Annulé-Canceled" THEN NULL
        WHEN Q1.l_lc = "Terminé-Completed" THEN "purple"
        WHEN Q1.l_lc = "Gelé-Frozen" THEN "frozen"
        WHEN Q1.l_ind = "Vert / Green" THEN "green"
        WHEN Q1.l_ind = "Orange" THEN "orange"
        WHEN Q1.l_ind = "Rouge / Red" THEN "red"
        ELSE "blue"
        END AS flag,
        CASE
        WHEN Q1.l_t_d IS NULL THEN NULL
        WHEN Q1.l_lc IN ("Terminé-Completed","Annulé-Canceled") THEN NULL
        WHEN Q1.l_t_d < NOW() THEN "Late"
        WHEN Q1.l_t_d > NOW() THEN "InTime"
        END AS tdlvstoday,

        CASE Q1.l_lc
        WHEN "Terminé-Completed" THEN true
        ELSE false
        END AS l_cplt,

 CASE
        WHEN Q1.l_lc IN ("Terminé-Completed", "Annulé-Canceled") THEN NULL
        WHEN Q1.l_lc IN ("En cours-In progress","Gelé-Frozen","A lancer-To be launched") AND PF.lifecycle = "Archivé-Archived" THEN "portfolio_archived"
        WHEN Q1.l_lc IN ("En cours-In progress","Gelé-Frozen","A lancer-To be launched") AND PROG.lifecycle = "Annulé-Canceled" THEN "program_canceled"
        WHEN Q1.l_lc IN ("En cours-In progress","Gelé-Frozen","A lancer-To be launched") AND PROG.lifecycle = "Terminé-Completed" THEN "portfolio_canceled"
        WHEN Q1.l_lc IN ("En cours-In progress","Gelé-Frozen","A lancer-To be launched") AND MPT.lifecycle = "Annulé-Canceled" THEN "mainproject_canceled"
        WHEN Q1.l_lc IN ("En cours-In progress","Gelé-Frozen","A lancer-To be launched") AND MPT.lifecycle = "Terminé-Completed" THEN "mainproject_completed"
        ELSE NULL
        END AS l_ctrl

FROM (

SELECT Q.*,
F.dataobject_name AS f_n,
F.dataobject_id AS f_id,
CASE
WHEN Q.mp IS NULL THEN Q.mp2
ELSE Q.mp
END AS mp_id

FROM

( SELECT L.dataobject_id AS l_id,
         L.dataobject_name AS l_n,
         L.lifecycle AS l_lc,
         L.$FinreelleActualend AS l_end,
         L.$DeadlinecibleTargetDeadline AS l_t_d,
         L. $DatedengagementCommitmentDate AS l_c_d,
         L.$AvancementProgress AS l_p,
         L.$IndicateurAvancement AS l_p_ind,
         L.$IndicateurLivrable AS l_ind,
         L.create_date AS l_cr_d,
         L.last_modified AS l_la_d,
         MP.dataobject_id AS mp,
         P.dataobject_parent_id AS mp2,

         CASE 
WHEN P. $_$PerimetreorgOrgscope LIKE '%;%' THEN NULL
WHEN MP. $_$PerimetreorgOrgscope LIKE '%;%' THEN NULL
WHEN P. $_$PerimetreorgOrgscope IS NOT NULL AND P.$_$PerimetreorgOrgscope NOT LIKE '%;%' THEN P.rel_dataobject_id
WHEN MP. $_$PerimetreorgOrgscope IS NOT NULL AND MP.$_$PerimetreorgOrgscope NOT LIKE '%;%' THEN MP.rel_dataobject_id
WHEN P. $_$PerimetreorgOrgscope IS NULL THEN NULL
WHEN MP. $_$PerimetreorgOrgscope IS NULL THEN NULL
END AS rel_id

        FROM tenant_tb_LIVRABLESDELIVERABLES L
        LEFT JOIN tenant_tb_3PROJETS_PROJECTS P ON L.dataobject_parent_id = P.dataobject_id
        LEFT JOIN tenant_tb_2CHANTIERS_MAINPROJECTS MP ON L.dataobject_parent_id = MP.dataobject_id) Q

        LEFT JOIN tenant_vw_dataobjects_rel_rel R ON R.dataobject_id = Q.rel_id 
        LEFT JOIN  tenant_tb_FORMAT F ON F.dataobject_id = R.related_dataobject_id 
) Q1

JOIN tenant_tb_2CHANTIERS_MAINPROJECTS MPT ON MPT.dataobject_id = mp_id
        JOIN tenant_tb_1PROGRAMMES_PROGRAMS PROG ON MPT.dataobject_parent_id= PROG.dataobject_id
        JOIN tenant_tb_PORTEFEUILLECORPPAYSAR PF ON PROG.dataobject_parent_id=PF.dataobject_id AND PF.dataobject_id <> 2099 

LEFT JOIN (
SELECT REL.dataobject_id AS rel_l_id,
         PR.dataobject_id AS pr_id,
         PR.dataobject_name AS pr_n,
         PR. $AxesstrategiquesStrategicaxis AS pr_stax,
         PR.$Star AS pr_star,
         PR.$PreuveSASAProof AS pr_sapr,
         PR.$PreuveunitaireUnitProof AS pr_un,
         PR.$SocleIT AS pr_it,
         PR.$EcheanceDGGDDeadline AS pr_dl,
         PR.lifecycle AS pr_lc,
         SMP.dataobject_id AS smp_id,
         SMP.dataobject_name AS smp_n,
         SMP.$ProprietaireOwner AS smp_o,
         SMP.lifecycle AS smp_lc,
         SPROG.dataobject_id AS spr_id,
         SPROG.dataobject_name AS spr_n,
         SPROG.lifecycle AS spr_lc
            FROM tenant_vw_dataobjects_rel_rel REL
            JOIN tenant_tb_PREUVESPROOFS PR ON REL.related_dataobject_id = PR.dataobject_id
            JOIN tenant_tb_CHANTIERSMAINPROJECTS SMP ON SMP.dataobject_id = PR.dataobject_parent_id
            JOIN tenant_tb_PROGRAMMESPROGRAMS SPROG ON SPROG.dataobject_id = SMP.dataobject_parent_id
            WHERE REL.relation_name = "$_$PreuvelieeRelatedproof" 
) Q2 ON Q2.rel_l_id = Q1.l_id )Q3

LIMIT 1000 OFFSET ##OFFSET##

