SELECT
L2.deliverable_id,
         L2.deliverable_name,
         L2.deliverable_actual_end,
         L2.deliverable_target_deadline,
         L2.deliverable_progress,
         L2.deliverable_progress_indicator,
         L2.deliverable_commitment_date,
         L2.deliverable_lifecycle,
         L2.mainproject_name,
         L2.mainproject_id,
         L2.program_name,
         L2.program_id,
         L2.territory_name,
         L2.territory_id,
L2.delete_data_control

FROM  
 (

SELECT L.deliverable_id,
         L.deliverable_name,
         L.deliverable_actual_end,
         L.deliverable_target_deadline,
         L.deliverable_progress,
         L.deliverable_progress_indicator,
         L.deliverable_commitment_date,
         L.deliverable_lifecycle,
L.deliverable_create_date AS deliverable_create_date,
L.deliverable_last_modified AS deliverable_last_modified,
         MP.create_date AS mainproject_create_date,
MP.last_modified AS mainproject_last_modified,
PROG.create_date AS program_create_date,
PROG.last_modified AS program_last_modified,
PORTFOLIO.create_date AS portfolio_create_date,
PORTFOLIO.last_modified AS portfolio_last_modified,
         MP.dataobject_name AS mainproject_name,
         MP.dataobject_id AS mainproject_id,
         PROG.dataobject_name AS program_name,
         PROG.dataobject_id AS program_id,
         PORTFOLIO.dataobject_name AS territory_name,
         PORTFOLIO.dataobject_id AS territory_id,

    CASE L.deliverable_lifecycle
    WHEN "Terminé-Completed" THEN
    L.deliverable_actual_end
    ELSE L.deliverable_target_deadline
    END AS "deliverable_expecteddate",

    CASE L.deliverable_indicator
    WHEN "Vert / Green" THEN
    "green"
    WHEN "Orange" THEN
    "orange"
    WHEN "Rouge / Red" THEN
    "red"
    ELSE "blue"
    END AS "flag",

    CASE 
    WHEN L.deliverable_commitment_date IS NULL THEN null
    WHEN L.deliverable_target_deadline IS NULL THEN null
    WHEN L.deliverable_commitment_date < L.deliverable_target_deadline THEN "Late"
    ELSE "In Time"
    END AS "TargetVsCommitment",


CASE L.deliverable_lifecycle
WHEN "Terminé-Completed" THEN true
ELSE false
END AS "deliverable_completed",

CASE L.deliverable_lifecycle
    WHEN "Terminé-Completed" THEN
    "valid"
    ELSE
CASE
    WHEN PORTFOLIO.lifecycle = "Archivé-Archived"
        OR PORTFOLIO.lifecycle = "Annulé-Canceled" THEN
    "error"
    WHEN PROG.lifecycle = "Archivé-Archived"
        OR PROG.lifecycle = "Annulé-Canceled" THEN
    "error"
    WHEN MP.lifecycle = "Archivé-Archived"
        OR MP.lifecycle = "Annulé-Canceled" THEN
    "error" 
    ELSE "valid"
END
    END AS delete_data_control

FROM 
    (SELECT DELIVERABLES. dataobject_id AS deliverable_id,
         DELIVERABLES.dataobject_name AS deliverable_name,
         DELIVERABLES.lifecycle AS deliverable_lifecycle,
         DELIVERABLES.$FinreelleActualend AS deliverable_actual_end,
         DELIVERABLES.$DeadlinecibleTargetDeadline AS deliverable_target_deadline,
         DELIVERABLES. $DatedengagementCommitmentDate AS deliverable_commitment_date,
         DELIVERABLES.$AvancementProgress AS deliverable_progress,
         DELIVERABLES.$IndicateurAvancement AS deliverable_progress_indicator,
         DELIVERABLES.$IndicateurLivrable AS deliverable_indicator,
         PROJETS.dataobject_name AS project_name,
         PROJETS.dataobject_id AS project_id,
         PROJETS. dataobject_parent_id AS mainproject_id_from_project,
         MAINPROJECTS.dataobject_id AS mainproject_id,
         DELIVERABLES.create_date AS deliverable_create_date,
         DELIVERABLES.last_modified AS deliverable_last_modified


    FROM tenant_vw_LIVRABLESDELIVERABLES DELIVERABLES

    LEFT JOIN tenant_vw_3PROJETS_PROJECTS PROJETS
        ON DELIVERABLES.dataobject_parent_id= PROJETS.dataobject_id
    LEFT JOIN tenant_vw_2CHANTIERS_MAINPROJECTS MAINPROJECTS
        ON DELIVERABLES.dataobject_parent_id= MAINPROJECTS.dataobject_id
    WHERE DELIVERABLES.lifecycle != "Annulé-Canceled") L
JOIN tenant_vw_2CHANTIERS_MAINPROJECTS MP
    ON MP.dataobject_id = COALESCE(L.mainproject_id_from_project, L.mainproject_id)
JOIN tenant_vw_1PROGRAMMES_PROGRAMS PROG
    ON MP.dataobject_parent_id= PROG.dataobject_id
JOIN tenant_vw_PORTEFEUILLECORPPAYSAR PORTFOLIO
    ON PROG.dataobject_parent_id=PORTFOLIO.dataobject_id
        AND PORTFOLIO.dataobject_id <> 2099
WHERE L.deliverable_create_date >= ##DATE##
        OR L.deliverable_last_modified >= ##DATE##
        OR MP.create_date >= ##DATE##
        OR MP.last_modified >= ##DATE##
        OR PROG.create_date >= ##DATE##
        OR PROG.last_modified >= ##DATE##
        OR PORTFOLIO.create_date >= ##DATE##
        OR PORTFOLIO.last_modified >= ##DATE##)L2

